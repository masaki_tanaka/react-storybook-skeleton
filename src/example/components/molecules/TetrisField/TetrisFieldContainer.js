import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import TetrisField from './TetrisField'
import {
  initActivePoints,
  initFieldMap,
  initMaterial,
  moveMaterialToX,
  moveMaterialToY,
  rotateMaterial,
  startDrop
} from '../../../../reducers/example/Tetris/index'

const stateToProps = state => {
  return {
    ...state.exampleTetris
  }
}

const dispatchToProps = dispatch => {
  return {
    ...bindActionCreators(
      {
        initMaterial,
        initFieldMap,
        initActivePoints,
        startDrop,
        moveMaterialToX,
        moveMaterialToY,
        rotateMaterial
      },
      dispatch
    )
  }
}

export default connect(stateToProps, dispatchToProps)(TetrisField)
