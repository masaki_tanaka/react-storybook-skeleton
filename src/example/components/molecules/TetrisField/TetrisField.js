import React from 'react'
import './style/index.css'
import TetrisMaterial from '../TetrisMaterial'
import PropTypes from 'prop-types'
import TetrisBlock from '../../atoms/TetrisBlock/TetrisBlock'

class TetrisField extends React.Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.props.initMaterial()
    this.props.initFieldMap()
  }

  componentWillReceiveProps({ type, rotationNum, isStop, isBreak }) {
    if (isStop) {
      this.props.startDrop()
      this.props.initActivePoints(type, rotationNum)
      this.start()
      return
    }

    if (isBreak) {
      this.stop()
      this.props.initMaterial()
    }
  }

  start() {
    this.dropping = setInterval(() => {
      const distance = 1
      this.props.moveMaterialToY(
        this.props.activePoints,
        this.props.fieldMap,
        this.props.materialPosition,
        distance
      )
    }, 1000)
  }

  stop() {
    clearInterval(this.dropping)
  }

  render() {
    const {
      type,
      rotationNum,
      materialPosition,
      fieldMap,
      activePoints,
      moveMaterialToX,
      rotateMaterial
    } = this.props
    const printBlocks = []
    for (let x in fieldMap) {
      for (let y in fieldMap[x]) {
        if (fieldMap[x][y]) {
          printBlocks.push({ x: x, y: y })
        }
      }
    }
    const tetrisBlocks = printBlocks.map((position, index) => {
      return (
        <TetrisBlock
          key={index}
          positionX={parseInt(position.x)}
          positionY={parseInt(position.y)}
        />
      )
    })
    const handleMaterial = e => {
      if (e.keyCode === 37) {
        const distance = -1
        moveMaterialToX(activePoints, fieldMap, materialPosition, distance)
        return
      }
      if (e.keyCode === 39) {
        const distance = 1
        moveMaterialToX(activePoints, fieldMap, materialPosition, distance)
        return
      }
      if (e.keyCode === 38) {
        const nextRotationNum = rotationNum + 1
        rotateMaterial(fieldMap, materialPosition, type, nextRotationNum)
      }
    }

    return (
      <div className={'tetris-field'} onKeyDown={handleMaterial} tabIndex={'0'}>
        {tetrisBlocks}
        <TetrisMaterial
          type={type}
          rotationNum={rotationNum}
          positionX={materialPosition.x}
          positionY={materialPosition.y}
        />
        <div className={'tetris-field-left-wall'} />
        <div className={'tetris-field-right-wall'} />
        <div className={'tetris-field-ground'} />
      </div>
    )
  }
}
TetrisField.propTypes = {
  type: PropTypes.string.isRequired,
  rotationNum: PropTypes.number.isRequired,
  materialPosition: PropTypes.object.isRequired,
  initMaterial: PropTypes.func.isRequired,
  initFieldMap: PropTypes.func.isRequired,
  moveMaterialToY: PropTypes.func.isRequired,
  moveMaterialToX: PropTypes.func.isRequired,
  activePoints: PropTypes.array.isRequired,
  rotateMaterial: PropTypes.func.isRequired
}

export default TetrisField
