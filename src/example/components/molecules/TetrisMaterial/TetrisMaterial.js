import React from 'react'
import PropTypes from 'prop-types'
import TetrisBlock from '../../atoms/TetrisBlock'
import './style/index.css'

const TetrisMaterial = ({ type, rotationNum, positionX, positionY }) => {
  const classes = {
    tetrisMaterial: ['tetris-material'],
    tetrisBlocks: [
      `tetris-block-${type}-${rotationNum}-1`,
      `tetris-block-${type}-${rotationNum}-2`,
      `tetris-block-${type}-${rotationNum}-3`,
      `tetris-block-${type}-${rotationNum}-4`
    ]
  }

  const tetrisBlocks = classes.tetrisBlocks.map((className, index) => (
    <TetrisBlock key={index} className={className} />
  ))

  const style = {
    left: `${15 * positionX}px`,
    top: `${15 * positionY}px`
  }

  return (
    <div className={classes.tetrisMaterial} style={style}>
      {tetrisBlocks}
    </div>
  )
}

TetrisMaterial.propTypes = {
  type: PropTypes.string.isRequired,
  rotationNum: PropTypes.number.isRequired,
  positionX: PropTypes.number.isRequired,
  positionY: PropTypes.number.isRequired
}

export default TetrisMaterial
