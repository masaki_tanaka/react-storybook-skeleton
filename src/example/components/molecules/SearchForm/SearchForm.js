import React from 'react'
import { Field, reduxForm } from 'redux-form'

const SearchForm = () => {
  return (
    <form className={'search-form'}>
      <h2>検索エリア</h2>
      <h3>ID</h3>
      <div>
        1
        <Field name="id" component="input" type="radio" value={'1'} />
      </div>
      <div>
        2
        <Field name="id" component="input" type="radio" value={'2'} />
      </div>
      <div>
        3
        <Field name="id" component="input" type="radio" value={'3'} />
      </div>
    </form>
  )
}

export default reduxForm({
  form: 'searchEngine'
})(SearchForm)
