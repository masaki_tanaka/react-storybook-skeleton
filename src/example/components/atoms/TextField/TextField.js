import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'

const renderField = ({
  input,
  placeholder,
  meta: { touched, error, warning }
}) => {
  const validationState = error ? 'error' : warning ? 'warning' : 'success'
  return (
    <Fragment>
      <input
        {...input}
        type="text"
        placeholder={placeholder}
        className={validationState}
      />
      {touched && error && <p>{error}</p>}
    </Fragment>
  )
}

renderField.propTypes = {
  input: PropTypes.object,
  placeholder: PropTypes.string,
  meta: PropTypes.object
}

const TextField = props => {
  return <Field {...props} component={renderField} />
}

export default TextField
