import React from 'react'
import PropTypes from 'prop-types'
import './style/index.css'

const TetrisBlock = ({ className, positionX, positionY }) => {
  const styles = {
    left: `${15 * positionX}px`,
    top: `${15 * positionY}px`
  }

  return <div className={`tetris-block ${className}`} style={styles} />
}

TetrisBlock.propTypes = {
  className: PropTypes.string,
  positionX: PropTypes.number,
  positionY: PropTypes.number
}

export default TetrisBlock
