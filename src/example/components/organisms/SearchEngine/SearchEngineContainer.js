import { connect } from 'react-redux'
import SearchEngine from './SearchEngine'
import { bindActionCreators } from 'redux'
import { fetchContents } from '../../../../reducers/example/Contents'
import { getFormValues } from 'redux-form'

const stateToProps = state => {
  return {
    contents: state.exampleContents.contents,
    formValues: getFormValues('searchEngine')(state)
  }
}

const dispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ fetchContents }, dispatch)
  }
}
export default connect(stateToProps, dispatchToProps)(SearchEngine)
