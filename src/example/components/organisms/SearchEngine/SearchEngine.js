import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { reduxForm } from 'redux-form'
import SearchForm from '../../molecules/SearchForm'

class SearchEngine extends React.Component {
  constructor(props) {
    super(props)

    this.fetchContents = this.props.fetchContents.bind(this)
  }
  componentWillMount() {
    this.fetchContents('http://localhost:3001/test-api?')
  }

  componentWillReceiveProps({ formValues }) {
    if (this.props.formValues !== formValues) {
      this.fetchContents('http://localhost:3001/test-api?', formValues)
    }
  }

  render() {
    const { contents } = this.props
    return (
      <Fragment>
        <SearchForm />
        <div className={'result-field'}>
          <h2>検索結果</h2>
          <ul>
            {contents.map((content, i) => {
              return (
                <li key={i}>
                  <p>id: {content.id}</p>
                  <p>
                    name: {content.name.first} {content.name.last}
                  </p>
                  <p>phone: {content.phone}</p>
                  <p>email: {content.email}</p>
                  <p>age: {content.age}</p>
                </li>
              )
            })}
          </ul>
        </div>
      </Fragment>
    )
  }
}

SearchEngine.propTypes = {
  contents: PropTypes.array
}

export default reduxForm({
  form: 'searchEngine'
})(SearchEngine)
