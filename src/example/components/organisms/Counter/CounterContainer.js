import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { increment } from '../../../../reducers/example/Counter'
import Counter from './Counter'

const stateToProps = state => {
  return {
    count: state.exampleCounter.count
  }
}

const dispatchToProps = dispatch => {
  return {
    ...bindActionCreators({ increment }, dispatch)
  }
}

export default connect(stateToProps, dispatchToProps)(Counter)
