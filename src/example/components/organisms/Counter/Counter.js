import React from 'react'
import PropTypes from 'prop-types'

const Counter = ({ count, increment }) => {
  return (
    <div>
      <p>count: {count}</p>
      <button type="button" onClick={() => increment(count)}>
        +
      </button>
    </div>
  )
}

Counter.propsTypes = {
  count: PropTypes.number.required,
  increment: PropTypes.func.required
}

export default Counter
