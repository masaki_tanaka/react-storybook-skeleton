import { combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'
import exampleCounter from './example/Counter'
import exampleContents from './example/Contents'
import exampleTetris from './example/Tetris/index'

export default combineReducers({
  exampleCounter,
  exampleContents,
  exampleTetris,
  form: reduxFormReducer
})
