import axios from 'axios'
import { createReducer } from '../../../utils'

const TYPE_PREFIX = '@example/Contents/'

/** initState */
const initState = {
  contents: []
}

/** actions */
export const fetchContents = (url, params) => {
  return dispatch => {
    return axios.get(url, {params: params}).then(response => {
      return dispatch({
        type: `${TYPE_PREFIX}FETCH_CONTENTS_SUCCESS`,
        payload: {contents: response.data}
      })
    })
  }
}

/** reducer */
export default createReducer(TYPE_PREFIX, initState)
