import { increment } from '../index'

describe('increment', () => {
  it('should increment count', () => {
    const count = 1
    const expected = { type: 'INCREMENT', payload: {count: 2} }
    expect(increment(count)).toEqual(expected)
  })
})
