import { createReducer } from '../../../utils'

const TYPE_PREFIX = '@/example/Counter/'

/** initState */
const initState = {
  count: 0
}

/** actions */
export const increment = count => {
  const nextCount = count + 1

  return {type: `${TYPE_PREFIX}INCREMENT`, payload: {count: nextCount}}
}

/** reducer */
export default createReducer(TYPE_PREFIX, initState)
