import { merge } from 'icepick'
import { createReducer } from '../../../utils'

const TYPE_PREFIX = '@example/Tetris/'

/** initState */
const initState = {
  type: '',
  rotationNum: 0,
  materialPosition: {
    x: 5,
    y: 0
  },
  fieldMap: [],
  activePoints: [
    {
      x: 0,
      y: 0
    },
    {
      x: 0,
      y: 0
    },
    {
      x: 0,
      y: 0
    },
    {
      x: 0,
      y: 0
    }
  ],
  isStop: true,
  isBreak: false
}

/** actions */
export const initMaterial = () => {
  const materialTypes = [
    'light-blue',
    'yellow',
    'purple',
    'blue',
    'orange',
    'green',
    'red'
  ]
  const materialType =
    materialTypes[Math.floor(Math.random() * materialTypes.length)]
  const payload = {
    type: materialType,
    rotationNum: initState.rotationNum,
    materialPosition: initState.materialPosition,
    isStop: true,
    isBreak: false
  }

  return {type: `${TYPE_PREFIX}INIT_MATERIAL`, payload}
}

export const initFieldMap = () => {
  const initFieldMap = []
  const maxXPoint = 12
  const maxYPoint = 24

  for (let x = 0; x < maxXPoint; x++) {
    initFieldMap[x] = []
    for (let y = 0; y < maxYPoint; y++) {
      if (x === 0 || x === maxXPoint - 1 || y === maxYPoint - 1) {
        initFieldMap[x][y] = true
        continue
      }
      initFieldMap[x][y] = false
    }
  }
  const payload = {fieldMap: initFieldMap}

  return {type: `${TYPE_PREFIX}INIT_FIELD_MAP`, payload}
}

export const initActivePoints = (type, rotationNum) => {
  const basePoints = materialPoints[type][rotationNum]
  const activePoints = basePoints.map(basePoint => {
    return {
      x: basePoint.x + initState.materialPosition.x,
      y: basePoint.y + initState.materialPosition.y
    }
  })

  const payload = {activePoints}

  return {type: `${TYPE_PREFIX}INIT_ACTIVE_POINTS`, payload}
}

export const startDrop = () => {
  const payload = {isStop: false}

  return {type: `${TYPE_PREFIX}START_DROP`, payload}
}

export const moveMaterialToY = (
  activePoints,
  fieldMap,
  materialPosition,
  distance) => {
  const nextMaterialPosition = Object.assign({}, materialPosition, {
    y: materialPosition.y + distance
  })
  const nextActivePoints = activePoints.map(activePoint => {
    return Object.assign({}, activePoint, {y: activePoint.y + distance})
  })
  if (!canMoveMaterial(nextActivePoints, fieldMap)) {
    const nextFieldMap = updateFieldMap(fieldMap, activePoints)
    const payload = {
      fieldMap: nextFieldMap,
      isBreak: true
    }
    return {type: `${TYPE_PREFIX}UPDATE_FIELD_MAP`, payload}
  }
  const payload = {
    activePoints: nextActivePoints,
    materialPosition: nextMaterialPosition
  }

  return {type: `${TYPE_PREFIX}MOVE_MATERIAL`, payload}
}

export const moveMaterialToX = (
  activePoints,
  fieldMap,
  materialPosition,
  distance) => {
  const nextMaterialPosition = Object.assign({}, materialPosition, {
    x: materialPosition.x + distance
  })
  const nextActivePoints = activePoints.map(
    activePoint => merge(activePoint, {x: activePoint.x + distance})
  )
  if (!canMoveMaterial(nextActivePoints, fieldMap)) {
    return {type: `${TYPE_PREFIX}NONE`}
  }
  const payload = {
    activePoints: nextActivePoints,
    materialPosition: nextMaterialPosition
  }

  return {type: `${TYPE_PREFIX}MOVE_MATERIAL`, payload}
}

export const rotateMaterial = (
  fieldMap,
  materialPosition,
  type,
  rotationNum) => {
  // TODO refactoring もろもろDRYへ
  const maxRotationNum = 4
  const nextRotationNum =
    rotationNum > 0
      ? rotationNum % maxRotationNum
      : -rotationNum % maxRotationNum
  const basePoints = materialPoints[type][nextRotationNum]
  const nextActivePoints = basePoints.map(basePoint => {
    return {
      x: basePoint.x + materialPosition.x,
      y: basePoint.y + materialPosition.y
    }
  })
  if (!canMoveMaterial(nextActivePoints, fieldMap)) {
    return {type: `${TYPE_PREFIX}NONE`}
  }
  const payload = {
    activePoints: nextActivePoints,
    rotationNum: nextRotationNum
  }
  return {type: `${TYPE_PREFIX}ROTATE_MATERIAL`, payload}
}

/** reducer */
export default createReducer(TYPE_PREFIX, initState)

const updateFieldMap = (fieldMap, activePoints) => {
  const copiedFieldMap = JSON.parse(JSON.stringify(fieldMap))
  for (const point of activePoints) {
    copiedFieldMap[point.x][point.y] = true
  }
  return copiedFieldMap
}
const canMoveMaterial = (nextActivePoints, fieldMap) => {
  for (const point of nextActivePoints) {
    if (fieldMap[point.x][point.y]) {
      return false
    }
  }
  return true
}

const lightBlue1 = [
  {
    x: 0,
    y: 3
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 0,
    y: 1
  },
  {
    x: 0,
    y: 0
  }
]
const lightBlue2 = [
  {
    x: 0,
    y: 2
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 2,
    y: 2
  },
  {
    x: 3,
    y: 2
  }
]
const yellow = [
  {
    x: 0,
    y: 3
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 1,
    y: 3
  },
  {
    x: 1,
    y: 2
  }
]
const purple1 = [
  {
    x: 1,
    y: 1
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 2,
    y: 2
  }
]
const purple2 = [
  {
    x: 0,
    y: 2
  },
  {
    x: 1,
    y: 3
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 1,
    y: 1
  }
]
const purple3 = [
  {
    x: 1,
    y: 2
  },
  {
    x: 0,
    y: 1
  },
  {
    x: 1,
    y: 1
  },
  {
    x: 2,
    y: 1
  }
]
const purple4 = [
  {
    x: 1,
    y: 2
  },
  {
    x: 0,
    y: 3
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 0,
    y: 1
  }
]
const blue1 = [
  {
    x: 0,
    y: 1
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 2,
    y: 2
  }
]
const blue2 = [
  {
    x: 1,
    y: 1
  },
  {
    x: 0,
    y: 3
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 0,
    y: 1
  }
]
const blue3 = [
  {
    x: 2,
    y: 2
  },
  {
    x: 2,
    y: 1
  },
  {
    x: 1,
    y: 1
  },
  {
    x: 0,
    y: 1
  }
]
const blue4 = [
  {
    x: 0,
    y: 3
  },
  {
    x: 1,
    y: 1
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 1,
    y: 3
  }
]
const orange1 = [
  {
    x: 2,
    y: 1
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 2,
    y: 2
  }
]
const orange2 = [
  {
    x: 1,
    y: 3
  },
  {
    x: 0,
    y: 3
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 0,
    y: 1
  }
]
const orange3 = [
  {
    x: 0,
    y: 2
  },
  {
    x: 2,
    y: 1
  },
  {
    x: 1,
    y: 1
  },
  {
    x: 0,
    y: 1
  }
]
const orange4 = [
  {
    x: 0,
    y: 1
  },
  {
    x: 1,
    y: 1
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 1,
    y: 3
  }
]
const green1 = [
  {
    x: 2,
    y: 2
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 1,
    y: 3
  },
  {
    x: 0,
    y: 3
  }
]
const green2 = [
  {
    x: 0,
    y: 1
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 1,
    y: 3
  }
]
const red1 = [
  {
    x: 2,
    y: 3
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 1,
    y: 3
  },
  {
    x: 0,
    y: 2
  }
]
const red2 = [
  {
    x: 0,
    y: 3
  },
  {
    x: 0,
    y: 2
  },
  {
    x: 1,
    y: 2
  },
  {
    x: 1,
    y: 1
  }
]
const materialPoints = {
  'light-blue': {
    0: lightBlue1,
    1: lightBlue2,
    2: lightBlue1,
    3: lightBlue2
  },
  yellow: {
    0: yellow,
    1: yellow,
    2: yellow,
    3: yellow
  },
  purple: {
    0: purple1,
    1: purple2,
    2: purple3,
    3: purple4
  },
  blue: {
    0: blue1,
    1: blue2,
    2: blue3,
    3: blue4
  },
  orange: {
    0: orange1,
    1: orange2,
    2: orange3,
    3: orange4
  },
  green: {
    0: green1,
    1: green2,
    2: green1,
    3: green2
  },
  red: {
    0: red1,
    1: red2,
    2: red1,
    3: red2
  }
}
