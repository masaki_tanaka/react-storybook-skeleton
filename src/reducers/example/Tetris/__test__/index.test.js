import {
  initActivePoints, initFieldMap, moveMaterialToX, moveMaterialToY,
  startDrop
} from '../index'
import {
  initActivePointsEnv, initFieldMapEnv, moveMaterialToXEnv,
  moveMaterialToYEnv
} from '../../../../../example-test.env'

describe('initFieldMap', () => {
  it('should init field map', () => {
    const payload = {fieldMap: initFieldMapEnv.fieldMap}
    const expected = { type: 'INIT_FIELD_MAP', payload }

    expect(initFieldMap()).toEqual(expected)
  })
})

describe('initActivePoints', () => {
  it('all materialType and rotationNum initialize', () => {
    const initRotationNum = 0
    for (const materialType of initActivePointsEnv.materialTypes) {
      const activePoints = initActivePointsEnv.activeMaterialPoints[materialType]
      const payload = {activePoints}
      const expected = { type: 'INIT_ACTIVE_POINTS', payload }

      expect(initActivePoints(materialType, initRotationNum)).toEqual(expected)
    }
  })
})

describe('startDrop', () => {
  it('return only type', () => {
    const payload = {isStop: false}
    const expected = { type: 'START_DROP', payload }
    expect(startDrop()).toEqual(expected)
  })
})

describe('moveMaterialToY', () => {
  it('can move material', () => {
    const activePoints = initActivePointsEnv.activeMaterialPoints['light-blue']
    const fieldMap = initFieldMapEnv.fieldMap
    const materialPosition = {x: 5, y: 0}
    const distance = 1
    const nextActivePoints = [
        {
          x: 5,
          y: 4
        },
        {
          x: 5,
          y: 3
        },
        {
          x: 5,
          y: 2
        },
        {
          x: 5,
          y: 1
        }
      ]
    const nextMaterialPosition = {x: 5, y: 1}
    const payload = {
      activePoints: nextActivePoints,
      materialPosition: nextMaterialPosition
    }
    const expected = {type: 'MOVE_MATERIAL', payload}

    expect(moveMaterialToY(activePoints, fieldMap, materialPosition, distance)).toEqual(expected)
  })

  it('can not move material', () => {
    const activePoints = moveMaterialToYEnv.canNotMoveMaterial.activeMaterialPoints
    const fieldMap = initFieldMapEnv.fieldMap
    const materialPosition = moveMaterialToYEnv.canNotMoveMaterial.materialPosition
    const distance = 1

    const nextFieldMap = Object.assign([], fieldMap.map(field => Object.assign([], field)))
    for (const point of activePoints) {
      nextFieldMap[point.x][point.y] = true
    }

    const payload = {
      fieldMap: nextFieldMap,
      isBreak: true
    }

    const expected = {type: 'UPDATE_FIELD_MAP', payload}

    expect(moveMaterialToY(activePoints, fieldMap, materialPosition, distance)).toEqual(expected)
  })
})

describe('moveMaterialToX', () => {
  it('can move material', () => {
    const activePoints = initActivePointsEnv.activeMaterialPoints['light-blue']
    const fieldMap = initFieldMapEnv.fieldMap
    const materialPosition = {x: 5, y: 0}
    const distance = 1

    const nextActivePoints = [
      {
        x: 6,
        y: 3
      },
      {
        x: 6,
        y: 2
      },
      {
        x: 6,
        y: 1
      },
      {
        x: 6,
        y: 0
      }
    ]
    const nextMaterialPosition = {x: 6, y: 0}
    const payload = {
      activePoints: nextActivePoints,
      materialPosition: nextMaterialPosition
    }

    const expected = {type: 'MOVE_MATERIAL', payload}

    expect(moveMaterialToX(activePoints, fieldMap, materialPosition, distance)).toEqual(expected)
  })
  it('return only type', () => {
    const activePoints = moveMaterialToXEnv.canNotMoveMaterial.activeMaterialPoints
    const fieldMap = initFieldMapEnv.fieldMap
    const materialPosition = moveMaterialToXEnv.canNotMoveMaterial.materialPosition
    const distance = 1

    const expected = {type: 'NONE'}

    expect(moveMaterialToX(activePoints, fieldMap, materialPosition, distance)).toEqual(expected)
  })
})

describe('rotateMaterial', () => {
  it('', () => {

  })
  it('return only type', () => {
    // const expected = { type: 'START_DROP' }
    // expect(startDrop()).toEqual(expected)
  })
})
