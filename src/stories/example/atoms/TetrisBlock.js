import React from 'react'

import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import TetrisBlock from '../../../example/components/atoms/TetrisBlock'

storiesOf('example/atoms', module).add(
  'TetrisBlock',
  withInfo()(() => <TetrisBlock />)
)
