import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { withInfo } from '@storybook/addon-info'
import { reduxForm } from 'redux-form'
import TextField from '../../../example/components/atoms/TextField'

const Story = reduxForm({
  form: 'textField',
  validate: null
})(TextField)

storiesOf('example/atoms', module).addWithRedux(
  'TextField',
  withInfo()(() => <Story name="story" onChange={action('change')} />)
)
