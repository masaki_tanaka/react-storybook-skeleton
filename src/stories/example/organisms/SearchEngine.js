import React from 'react'

import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import SearchEngine from '../../../example/components/organisms/SearchEngine'

storiesOf('example/organisms', module).addWithRedux(
  'SearchEngine',
  withInfo()(() => <SearchEngine />)
)
