import React from 'react'

import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import Counter from '../../../example/components/organisms/Counter'

storiesOf('example/organisms', module).addWithRedux(
  'Counter',
  withInfo()(() => <Counter />)
)
