import React from 'react'

import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import { withKnobs, select, number } from '@storybook/addon-knobs/react'
import TetrisMaterial from '../../../example/components/molecules/TetrisMaterial/index'

storiesOf('example/molecules', module)
  .addDecorator(withKnobs)
  .add(
    'TetrisMaterial',
    withInfo()(() => {
      const typeOptions = {
        'light-blue': '水色',
        yellow: '黄色',
        purple: '紫色',
        blue: '青色',
        orange: 'オレンジ色',
        green: '緑色',
        red: '赤色'
      }
      const type = select('Type', typeOptions, 'light-blue')
      const rotationNum = number('回転', 1)

      const positionX = number('横移動', 0)

      const positionY = number('縦移動', 0)

      return (
        <TetrisMaterial
          type={type}
          rotationNum={parseInt(rotationNum)}
          positionX={positionX}
          positionY={positionY}
        />
      )
    })
  )
