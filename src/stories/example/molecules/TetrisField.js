import React from 'react'

import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import { withKnobs } from '@storybook/addon-knobs/react'
import TetrisField from '../../../example/components/molecules/TetrisField'

storiesOf('example/molecules', module)
  .addDecorator(withKnobs)
  .addWithRedux(
    'TetrisField',
    withInfo()(() => {
      return <TetrisField />
    })
  )
