import React from 'react'

import { storiesOf } from '@storybook/react'
import App from '../../../example/pages/App'

storiesOf('example/pages', module).add('App', () => <App />)
