import { merge } from 'icepick'

export const createReducer = (typePrefix, initState) => {
  return (state = initState, {type, payload}) => {
    if (!type.startsWith(typePrefix)) {
      return state
    }

    if (payload === undefined) {
      return state
    }
    console.log(type)

    return merge(state, payload)
  }
}
