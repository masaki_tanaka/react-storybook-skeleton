React-Redux-storybookの開発環境の雛形です。

## 導入手順
#### 任意のディレクトリでclone
```git
git clone 
```
#### 自身のプロジェクトに修正 
1. .gitディレクトリを削除
1. ベースディレクトリ名とpackage.json内のnameを任意のものに変更
1. gitファイル作成
```git
git init
```
#### パッケージインストール
```npm
npm install
```
#### 依存ツール
この雛形ではapiのテスト用に[json-server](https://github.com/typicode/json-server)を使っています。

その為グローバルのパッケージに[json-server](https://github.com/typicode/json-server)をインストールする必要があります
```npm
npm install -g json-server
```

## 開発サーバ起動方法
```npm
npm run start
```
## storybook起動方法(apiテストサーバも一緒に起動します)
```npm
npm run storybook
```

## 解説
この雛形は[create-react-app](https://github.com/facebook/create-react-app)をベースに作成しています。

コンポーネントは[Atomic Design](http://apbcss.com/)をベースのファイル構成です。

.gitkeepというファイルは空ディレクトリをコミットするために用意したものなので削除しても問題ありません。

使用例としてsrc配下、stories配下、reducer配下それぞれにexampleディレクトリを用意しています。

exampleが不要の場合はそれぞれのディレクトリを削除し以下のimportしているファイルから依存箇所を削除してください
```javascript
/* src/index.js */

// 4行目
import App from './example/pages/App'; // ←ここをプロジェクトのトップコンポーネントに修正
```
```javascript
/* reducers/index.js */
import example // example～の行を全て削除

export default combineReducers({ // 内のexampleとついているものを全て削除
```
```javascript
/* stories/index.js */
import './example' // ←削除
```

