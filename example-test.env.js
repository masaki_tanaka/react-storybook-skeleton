export const initFieldMapEnv = {
  fieldMap: [
    [
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      false,
      true
    ],
    [
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true,
      true
    ]
  ]
}

export const initActivePointsEnv = {
  materialTypes: [
    'light-blue',
    'yellow',
    'purple',
    'blue',
    'orange',
    'green',
    'red'
  ],
  activeMaterialPoints: {
    'light-blue': [
      {
        x: 5,
        y: 3
      },
      {
        x: 5,
        y: 2
      },
      {
        x: 5,
        y: 1
      },
      {
        x: 5,
        y: 0
      }
    ],
    yellow: [
      {
        x: 5,
        y: 3
      },
      {
        x: 5,
        y: 2
      },
      {
        x: 6,
        y: 3
      },
      {
        x: 6,
        y: 2
      }
    ],
    purple: [
      {
        x: 6,
        y: 1
      },
      {
        x: 5,
        y: 2
      },
      {
        x: 6,
        y: 2
      },
      {
        x: 7,
        y: 2
      }
    ],
    blue: [
      {
        x: 5,
        y: 1
      },
      {
        x: 5,
        y: 2
      },
      {
        x: 6,
        y: 2
      },
      {
        x: 7,
        y: 2
      }
    ],
    orange: [
      {
        x: 7,
        y: 1
      },
      {
        x: 5,
        y: 2
      },
      {
        x: 6,
        y: 2
      },
      {
        x: 7,
        y: 2
      }
    ],
    green: [
      {
        x: 7,
        y: 2
      },
      {
        x: 6,
        y: 2
      },
      {
        x: 6,
        y: 3
      },
      {
        x: 5,
        y: 3
      }
    ],
    red: [
      {
        x: 7,
        y: 3
      },
      {
        x: 6,
        y: 2
      },
      {
        x: 6,
        y: 3
      },
      {
        x: 5,
        y: 2
      }
    ]
  }
}

export const moveMaterialToYEnv = {
  canNotMoveMaterial: {
    activeMaterialPoints: [
      {
        x: 5,
        y: 22
      },
      {
        x: 5,
        y: 21
      },
      {
        x: 5,
        y: 20
      },
      {
        x: 5,
        y: 19
      }
    ],
    materialPosition: {x: 5, y: 19}
  }
}

export const moveMaterialToXEnv = {
  canNotMoveMaterial: {
    activeMaterialPoints: [
      {
        x: 10,
        y: 3
      },
      {
        x: 10,
        y: 2
      },
      {
        x: 10,
        y: 1
      },
      {
        x: 10,
        y: 0
      }
    ],
    materialPosition: {x: 10, y: 0}
  }
}
